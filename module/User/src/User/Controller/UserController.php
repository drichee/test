<?php

/**
 * Felhasználó Controller. 
 * Felhasználók listázása, szerkesztése, törlése, létrehozás, filefeltöltés, 
 * bejelentkezés és kijelentkezés
 * 
 * @author RD
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Model\User;
use User\Form\UserForm;
use User\Form\LoginForm;
use User\Form\UploadForm;

class UserController extends AbstractActionController {

    protected $userTable;
    protected $messages = array();

    /**
     * 
     * @return User\Model\UserTable userTable
     */
    public function getUserTable() {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

    /**
     * A kezdőoldali akció. 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {
            return new ViewModel(array(
                        'users' => $this->getUserTable()->fetchAll(),
                    ));
        } else {
            return $this->redirect()->toRoute('user', array(
                        'action' => 'login'
                    ));
        }
    }

    /**
     * Felhasználó hozzáadása akció. (Nem szükséges a bejelentkezés)
     * @return array Form és Messages
     */
    public function addAction() {
        $form = new UserForm();
        $form->get('submit')->setValue('Hozzáad');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = new User();
            $form->setInputFilter($user->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $user->exchangeArray($form->getData());
                $this->getUserTable()->saveUser($user);
                $this->messages[] = "Felhasználó sikeresen létrehozva";
                return array('messages' => $this->messages);
            }
        }
        return array('form' => $form, 'messages' => $this->messages);
    }

    /**
     * Felhasználó szerkesztése. Amennyiben nincs bejelentkezett felhasználó
     * akkor a kezdőoldalra navigál.
     * @return array EditForm és Messages
     */
    public function editAction() {
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!$id) {
                return $this->redirect()->toRoute('user', array(
                            'action' => 'add'
                        ));
            }

            try {
                $user = $this->getUserTable()->getUser($id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('user', array(
                            'action' => 'index'
                        ));
            }

            $form = new UserForm();
            $form->bind($user);
            $form->get('submit')->setAttribute('value', 'Mentés');

            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setInputFilter($user->getInputFilter());
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->getUserTable()->saveUser($form->getData());
                    return $this->redirect()->toRoute('user');
                }
            }
            return array(
                'id' => $id,
                'form' => $form,
            );
        } else {
            return $this->redirect()->toRoute('user', array("action" => "login"));
        }
    }

    /**
     * Lásd indexAction;
     * @return indexAction
     */
    public function listAction() {
        return $this->indexAction();
    }

    /**
     * Felhasználó törlésének folyamat. Bejelentkezett felhasználóval érhető 
     * csak el. Jóváhagyás szükséges a törléshez.
     * @return array
     */
    public function deleteAction() {
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {

            $id = (int) $this->params()->fromRoute('id', 0);
            if (!$id) {
                return $this->redirect()->toRoute('user');
            }

            $request = $this->getRequest();
            if ($request->isPost()) {
                $del = $request->getPost('del', 'No');

                if ($del == 'Yes') {
                    $id = (int) $request->getPost('id');
                    $this->getUserTable()->deleteUser($id);
                }
                return $this->redirect()->toRoute('user');
            }

            return array(
                'id' => $id,
                'user' => $this->getUserTable()->getUser($id)
            );
        } else {
            return $this->redirect()->toRoute('user', array("action" => "login"));
        }
    }

    /**
     * Bejelentkezés. Sikeres bejelentkezés esetén átnavigál a felhasználók 
     * listájára.
     * @return array
     */
    public function loginAction() {
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {
            return $this->redirect()->toRoute('user', array("action" => "logged"));
        } else {
            $form = new LoginForm();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $username = $request->getPost('username');
                $password = $request->getPost('password');
                echo $password;
                $user = $this->getUserTable()->getLoginUser($username, $password);

                if ($user != null) {
                    $_SESSION['logged'] = true;
                    $_SESSION['user_id'] = $user->id;
                    return $this->redirect()->toRoute('user', array("action" => "list"));
                } else {
                    $this->messages[] = 'Sikertelen bejelentkezés.';
                }
            }
            return array('form' => $form, 'messages' => $this->messages);
        }
    }

    /**
     * Kijelentkezés. Megszünteti a $_SESSION értékeket
     * @return indexAction
     */
    public function logoutAction() {
        unset($_SESSION['logged']);
        unset($_SESSION['user_id']);
        return $this->indexAction();
    }

    /**
     * Bejelentkezett felhasználó kezdőoldalja.
     * @return User
     */
    public function loggedAction() {
        if (isset($_SESSION['user_id']) && is_numeric($_SESSION['user_id'])) {
            $id = (int) $_SESSION['user_id'];
        }
        try {
            $user = $this->getUserTable()->getUser($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('user', array(
                        'action' => 'index'
                    ));
        }
        return array('user' => $user);
    }

    /**
     * Felhasználó feltöltő akció. Json file feltöltése és feldolgozása.
     * @return array sikeresen és sikertelenül feltöltött <i>User</i> tömbjei.
     */
    public function uploadAction() {
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {
            $form = new UploadForm();
            $failed = array();
            $success = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $file = $request->getFiles('file');
                if ($file['tmp_name'] != "") {
                    $users = $this->processJsonFile($file['tmp_name']);
                    $success = $users['success'];
                    $failed = $users['failed'];
                } else{
                    $this->messages[] = "Nem adtál meg semmilyen file-t.";
                }
            }
            return array('form' => $form, 'messages' => $this->messages, "success" => $success, "failed" => $failed);
        } else {
            return $this->redirect()->toRoute('user', array("action" => "login"));
        }
    }

    /**
     * Feldolgozza a megadott file tartlmát. Dekódolja a file tartalmát és létrehozza, 
     * majd egyenként elmenti a <i>User</i>-eket.
     * @param string $file feltöltött file útvonala.
     * @return array array sikeresen és sikertelenül feltöltött <i>User</i>-ek.
     */
    protected function processJsonFile($file) {
        $success = array();
        $failed = array();
        $string = file_get_contents($file);
        $json_a = json_decode($string, true);
        if ($json_a != null) {
            if (isset($json_a['users'])) {
                $json_users = $json_a['users'];
                foreach ($json_users as $json_user) {
                    $saved = true;
                    $user = new User();
                    $user->exchangeArray($json_user);
                    try {
                        $this->getUserTable()->saveUser($user);
                    } catch (\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
                        $saved = false;
                        $failed[] = $user;
                    }
                    if ($saved) {
                        $success[] = $user;
                    }
                }
            }
        } else {
            $this->messages[] = "Sikertelen betöltés. Rossz file formátum.";
        }
        return array("success" => $success, "failed" => $failed);
    }

}
