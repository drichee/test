<?php

namespace User\Form;

/**
 * Description of UserForm
 *
 * @author RD
 */
use Zend\Form\Form;

class UserForm extends Form {

    public function __construct($name = null) {
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Teljes név',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'options' => array(
                'label' => 'E-mail',
            ),
        ));
        $this->add(array(
            'name' => 'username',
            'type' => 'Text',
            'options' => array(
                'label' => 'Felhasználónév',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Jelszó',
            ),
        ));
        $this->add(array(
            'name' => 'role',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'role_selector'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Egyszerű felhasználó',
                    '1' => 'Karbantartó',
                    '2' => 'Adminisztrátor',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'class' => 'button',
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }

}

?>
